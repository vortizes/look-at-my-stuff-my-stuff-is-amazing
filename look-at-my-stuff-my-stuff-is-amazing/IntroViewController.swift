//
//  IntroViewController.swift
//  look-at-my-stuff-my-stuff-is-amazing
//
//  Created by Cesar Ortiz on 3/31/16.
//  Copyright © 2016 CORB. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didTapCloseButton(sender: AnyObject) {
        
        let notificationsVC = NotificationsViewController(nibName: "NotificationsViewController", bundle: nil)
        
        presentViewController(notificationsVC, animated: true, completion: nil)

//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        
//        appDelegate.setAnotherRootViewController(notificationsVC)
//        
//        self.navigationController?.pushViewController(notificationsVC, animated: true)
    }
}
