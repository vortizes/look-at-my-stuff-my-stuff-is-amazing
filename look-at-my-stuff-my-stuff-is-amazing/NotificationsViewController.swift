//
//  NotificationsViewController.swift
//  look-at-my-stuff-my-stuff-is-amazing
//
//  Created by Cesar Ortiz on 3/31/16.
//  Copyright © 2016 CORB. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func didTapCloseButton(sender: AnyObject) {
        
        let userLocationVC = UserLocationViewController(nibName: "UserLocationViewController", bundle: nil)
  
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.setAnotherRootViewController(userLocationVC)
    }
}
