//
//  CategoryCollectionViewController.swift
//  look-at-my-stuff-my-stuff-is-amazing
//
//  Created by Cesar Ortiz on 4/7/16.
//  Copyright © 2016 CORB. All rights reserved.
//

import UIKit

private let reuseIdentifier = "CategoryCell"

class CategoryCollectionViewController: UICollectionViewController {

    var mainContainerVC : MainContainerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationItem.title = "Categorías"
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell nib
        let nibName = UINib(nibName: "CategoryCollectionViewCell", bundle:nil)
        collectionView!.registerNib(nibName, forCellWithReuseIdentifier: reuseIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {

        return 3
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 3
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! CategoryCollectionViewCell
    
        var imgColor:UIColor = .init()
        var backgroundColor:UIColor = .init()
        
        if indexPath.section == 0 {
            
            switch indexPath.row {
            
            case 0:
                imgColor = UIColor(red: CGFloat(193/255.0), green: CGFloat(193/255.0), blue: CGFloat(193/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(255/255.0), green: CGFloat(255/255.0), blue: CGFloat(255/255.0), alpha: CGFloat(1.0))
                cell.title.textColor = .blackColor()
                cell.subtitle.textColor = UIColor(red: CGFloat(193/255.0), green: CGFloat(193/255.0), blue: CGFloat(193/255.0), alpha: CGFloat(1.0))
                break
            case 1:
                imgColor = UIColor(red: CGFloat(63/255.0), green: CGFloat(135/255.0), blue: CGFloat(181/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(82/255.0), green: CGFloat(179/255.0), blue: CGFloat(240/255.0), alpha: CGFloat(1.0))
                break
            case 2:
                imgColor = UIColor(red: CGFloat(88/255.0), green: CGFloat(83/255.0), blue: CGFloat(138/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(114/255.0), green: CGFloat(106/255.0), blue: CGFloat(180/255.0), alpha: CGFloat(1.0))
                break
            default:
                break
            }
            cell.imageView.backgroundColor = imgColor
            cell.backgroundColor = backgroundColor
        }
        if indexPath.section == 1 {
            
            switch indexPath.row {
                
            case 0:
                imgColor = UIColor(red: CGFloat(121/255.0), green: CGFloat(72/255.0), blue: CGFloat(126/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(159/255.0), green: CGFloat(94/255.0), blue: CGFloat(166/255.0), alpha: CGFloat(1.0))
                break
            case 1:
                imgColor = UIColor(red: CGFloat(164/255.0), green: CGFloat(117/255.0), blue: CGFloat(150/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(216/255.0), green: CGFloat(153/255.0), blue: CGFloat(199/255.0), alpha: CGFloat(1.0))
                break
            case 2:
                imgColor = UIColor(red: CGFloat(166/255.0), green: CGFloat(56/255.0), blue: CGFloat(53/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(219/255.0), green: CGFloat(72/255.0), blue: CGFloat(68/255.0), alpha: CGFloat(1.0))
                break
            default:
                break
            }
            cell.imageView.backgroundColor = imgColor
            cell.backgroundColor = backgroundColor
        }
        
        if indexPath.section == 2 {
            
            switch indexPath.row {
                
            case 0:
                imgColor = UIColor(red: CGFloat(174/255.0), green: CGFloat(75/255.0), blue: CGFloat(50/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(231/255.0), green: CGFloat(97/255.0), blue: CGFloat(64/255.0), alpha: CGFloat(1.0))
                break
            case 1:
                imgColor = UIColor(red: CGFloat(186/255.0), green: CGFloat(131/255.0), blue: CGFloat(79/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(245/255.0), green: CGFloat(171/255.0), blue: CGFloat(102/255.0), alpha: CGFloat(1.0))
                break
            case 2:
                imgColor = UIColor(red: CGFloat(182/255.0), green: CGFloat(146/255.0), blue: CGFloat(34/255.0), alpha: CGFloat(1.0))
                backgroundColor = UIColor(red: CGFloat(243/255.0), green: CGFloat(193/255.0), blue: CGFloat(44/255.0), alpha: CGFloat(1.0))
                break
            default:
                break
            }
            cell.imageView.backgroundColor = imgColor
            cell.backgroundColor = backgroundColor
        }
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    }

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
        let screenRect : CGRect = UIScreen.mainScreen().bounds
        let screenWidth : CGFloat = screenRect.size.width
        let cellWidth : CGFloat = screenWidth / 3.0
        let roundedF : CGFloat = CGFloat(floorf(Float(cellWidth)))
        let gap : CGFloat = 5.0
        let size : CGSize = CGSizeMake(roundedF-gap, 140.0)
        
        return size;
    }
}
