//
//  MainContainerViewController.swift
//  look-at-my-stuff-my-stuff-is-amazing
//
//  Created by Cesar Ortiz on 4/6/16.
//  Copyright © 2016 CORB. All rights reserved.
//

import UIKit
import ExpandingMenu

class MainContainerViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var containerView: UIView!

    var imagePicker: UIImagePickerController!
    var stuffVC : StuffViewController!
    var categoryVC : CategoryCollectionViewController!
    var profileVC : ProfileViewController!
    
    var viewIdentifier : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureExpandingMenuButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let screenRect : CGRect = UIScreen.mainScreen().bounds
        containerView.frame = screenRect
        
        // **************** stuffVC ****************
        // configurar el container view
        if stuffVC == nil {
            stuffVC = StuffViewController(nibName: "StuffViewController", bundle: NSBundle.mainBundle())
            
            // guardamos la referencia hacia el controlador padre
            stuffVC.mainContainerVC = self
            addChildViewController(stuffVC)
            stuffVC.view!.frame = containerView.frame
            view!.insertSubview(stuffVC.view!, aboveSubview: containerView)
            view!.contentMode = .ScaleAspectFit
            stuffVC.didMoveToParentViewController(self)
        }
        else {
            
            // en caso que un memory warning haya eliminado la vista del container, recargarla
            if !stuffVC.isViewLoaded() {
                addChildViewController(stuffVC)
                stuffVC.view!.frame = containerView.frame
                view!.insertSubview(stuffVC.view!, aboveSubview: containerView)
                stuffVC.didMoveToParentViewController(self)
            }
        }
        
        // **************** categoryVC ****************
        // configurar el container view
        if categoryVC == nil {
            categoryVC = CategoryCollectionViewController(nibName: "CategoryCollectionViewController", bundle: NSBundle.mainBundle())
            
            // guardamos la referencia hacia el controlador padre
            categoryVC.mainContainerVC = self
            addChildViewController(categoryVC)
            categoryVC.view!.frame = containerView.frame
            
            view!.insertSubview(categoryVC.view!, aboveSubview: containerView)
            view!.contentMode = .ScaleAspectFit
            categoryVC.didMoveToParentViewController(self)
        }
        else {
            
            // en caso que un memory warning haya eliminado la vista del container, recargarla
            if !categoryVC.isViewLoaded() {
                addChildViewController(categoryVC)
                categoryVC.view!.frame = containerView.frame
                view!.insertSubview(categoryVC.view!, aboveSubview: containerView)
                categoryVC.didMoveToParentViewController(self)
            }
        }
        
        // **************** profileVC ****************
        // configurar el container view
        if profileVC == nil {
            profileVC = ProfileViewController(nibName: "ProfileViewController", bundle: NSBundle.mainBundle())
            
            // guardamos la referencia hacia el controlador padre
            profileVC.mainContainerVC = self
            addChildViewController(profileVC)
            profileVC.view!.frame = containerView.frame
            view!.insertSubview(profileVC.view!, aboveSubview: containerView)
            profileVC.didMoveToParentViewController(self)
        }
        else {
            
            // en caso que un memory warning haya eliminado la vista del container, recargarla
            if !profileVC.isViewLoaded() {
                addChildViewController(profileVC)
                profileVC.view!.frame = containerView.frame
                view!.insertSubview(profileVC.view!, aboveSubview: containerView)
                profileVC.didMoveToParentViewController(self)
            }
        }
        
        self.view!.layoutSubviews()
        
        displaySelectedView(viewIdentifier)
    }
    
    func displaySelectedView(viewId : Int) {
        
        switch viewId {
        
        case 1:
            stuffVC.view.hidden = false;
            categoryVC.view.hidden = true;
            profileVC.view.hidden = true;
            break
        case 2:
            stuffVC.view.hidden = true;
            categoryVC.view.hidden = false;
            profileVC.view.hidden = true;
            break
        case 3:
            break
        case 4:
            stuffVC.view.hidden = true;
            categoryVC.view.hidden = true;
            profileVC.view.hidden = false;
            break
        default:
            break
            
        }
    }
    
    // MARK: UIImagePickerController delegate method
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        
//        imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
    }
    
    private func configureExpandingMenuButton() {
        let menuButtonSize: CGSize = CGSize(width: 64.0, height: 64.0)
        let menuButton = ExpandingMenuButton(frame: CGRect(origin: CGPointZero, size: menuButtonSize), centerImage: UIImage(named: "chooser-button-tab")!, centerHighlightedImage: UIImage(named: "chooser-button-tab-highlighted")!)
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        menuButton.center = CGPointMake(screenSize.width - 32.0, screenSize.height - 92.0)
        self.view.addSubview(menuButton)
        
        func showAlert(title: String) {
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        let item1 = ExpandingMenuItem(size: menuButtonSize, title: "Crystal-Meth", image: UIImage(named: "crystalmeth")!, highlightedImage: UIImage(named: "crystalmeth")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            
            self.viewIdentifier = 1
        }
        
        let item2 = ExpandingMenuItem(size: menuButtonSize, title: "Cocaine", image: UIImage(named: "cocaine")!, highlightedImage: UIImage(named: "cocaine")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            
            self.viewIdentifier = 2
        }
        
        let item3 = ExpandingMenuItem(size: menuButtonSize, title: "Peyote-Camera", image: UIImage(named: "peyote")!, highlightedImage: UIImage(named: "peyote")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            
            self.imagePicker =  UIImagePickerController()
            self.imagePicker.delegate = self
            
            if !UIImagePickerController.isSourceTypeAvailable(.Camera) {
                
                self.imagePicker.sourceType = .PhotoLibrary
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
                
            } else {
                
                let actionSheetController: UIAlertController = UIAlertController(title: "Imagen", message: "¿Desde donde quieres cargar la imagen?", preferredStyle: .ActionSheet)
                
                let cameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .Default) { action -> Void in
                
                    self.imagePicker.sourceType = .Camera
                    self.presentViewController(self.imagePicker, animated: true, completion: nil)
                }
                actionSheetController.addAction(cameraAction)

                let libraryAction: UIAlertAction = UIAlertAction(title: "Library", style: .Default) { action -> Void in

                    self.imagePicker.sourceType = .PhotoLibrary
                    self.presentViewController(self.imagePicker, animated: true, completion: nil)
                }
                actionSheetController.addAction(libraryAction)
                
                let cancelAction: UIAlertAction = UIAlertAction(title: "Ok", style: .Cancel) { action -> Void in
                    //Just dismiss the action sheet
                }
                actionSheetController.addAction(cancelAction)
                
                self.presentViewController(actionSheetController, animated: true, completion: { _ in })
            }
        }
        
        let item4 = ExpandingMenuItem(size: menuButtonSize, title: "LSD", image: UIImage(named: "lsd")!, highlightedImage: UIImage(named: "lsd")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
        }
        
        let item5 = ExpandingMenuItem(size: menuButtonSize, title: "Sugar", image: UIImage(named: "sugar")!, highlightedImage: UIImage(named: "sugar")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            
            self.viewIdentifier = 4
        }
        
        menuButton.addMenuItems([item1, item2, item3, item4, item5])
        
        menuButton.willPresentMenuItems = { (menu) -> Void in
            print("MenuItems will present.")
        }
        
        menuButton.didDismissMenuItems = { (menu) -> Void in
            print("MenuItems dismissed.")
        }
    }
}
