//
//  CategoryCollectionViewCell.swift
//  look-at-my-stuff-my-stuff-is-amazing
//
//  Created by Cesar Ortiz on 4/7/16.
//  Copyright © 2016 CORB. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
