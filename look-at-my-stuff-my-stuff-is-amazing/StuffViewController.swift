//
//  StuffViewController.swift
//  look-at-my-stuff-my-stuff-is-amazing
//
//  Created by Cesar Ortiz on 3/31/16.
//  Copyright © 2016 CORB. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ProductCell"

class StuffViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    @IBOutlet weak var productCollectionView: UICollectionView!
    
    var mainContainerVC : MainContainerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nibName = UINib(nibName: reuseIdentifier, bundle:nil)
        
        productCollectionView.registerNib(nibName, forCellWithReuseIdentifier: reuseIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UICollectionView delegate methods
    func collectionView(collectionView: UICollectionView, numberOfItemInSection section: Int) -> Int{
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{

        let cell = productCollectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ProductCell
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("Cell \(indexPath.row) selected")
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            let screenRect : CGRect = UIScreen.mainScreen().bounds
            let screenWidth : CGFloat = screenRect.size.width
            let cellWidth : CGFloat = screenWidth / 2.0
            let roundedF : CGFloat = CGFloat(floorf(Float(cellWidth)))
            let gap : CGFloat = 5.0
            let size : CGSize = CGSizeMake(roundedF-gap*2, 417.0)
            
            return size;
    }

    
}
