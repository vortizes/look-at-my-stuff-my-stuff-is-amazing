//
//  UserLocationViewController.swift
//  look-at-my-stuff-my-stuff-is-amazing
//
//  Created by Cesar Ortiz on 3/31/16.
//  Copyright © 2016 CORB. All rights reserved.
//

import UIKit

class UserLocationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didTapCloseButton(sender: AnyObject) {
        
        let mainStuffVC = MainContainerViewController(nibName: "MainContainerViewController", bundle: nil)
        
        let navigationVC = UINavigationController(rootViewController: mainStuffVC)
        navigationVC.navigationBar.translucent = false
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.setAnotherRootViewController(navigationVC)
    }
}
